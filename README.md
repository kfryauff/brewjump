# Brew Jump #

This is the initial psd -> html/css/js project

### Included in this Repo ###

* Index Page : index.html
* Stylesheets : assets/stylesheets/*
	* Note: Sass files were here used to generate css
* Images : assets/images/*
	* Note: These are low res images taken from psd file

### How do I get set up? ###

To set up, you can run: 

``` npm install ```

or relink jquery and bootstrap in index.html to cdn version or local
(this will be changed after the layout is up).

### Current Project Contributor ###

* Krista Fryauff: krista.fryauff@gmail.com